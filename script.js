let btnTheme = document.querySelector('.theme')
let link = document.getElementById('theme-link')

btnTheme.addEventListener('click',ChengeTheme)

function ChengeTheme() {
    let whiteTheme = "./css/style_white.css"
    let blackTheme = "./css/style_black.css"

    let currTheme = link.getAttribute("href");
    let theme = "";

    if(currTheme == whiteTheme)
    {
        currTheme = blackTheme;
        theme = "black";
    }
    else
    {    
        currTheme = whiteTheme;
        theme = "white";
    }

    link.setAttribute("href", currTheme);

    localStorage.setItem("theme", theme)
}

function enterTheme(){
    let whiteTheme = "./css/style_white.css"
    let blackTheme = "./css/style_black.css"
    let currTheme = localStorage.getItem("theme");

if (currTheme == "black") {
    link.setAttribute("href", blackTheme);
} else {
    link.setAttribute("href", whiteTheme);
};
};
enterTheme();